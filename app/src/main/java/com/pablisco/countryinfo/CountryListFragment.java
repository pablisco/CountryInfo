package com.pablisco.countryinfo;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.ListFragment;
import android.support.v4.content.Loader;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.pablisco.countryinfo.adapters.CountryAdapter;
import com.pablisco.countryinfo.model.Country;
import com.pablisco.countryinfo.net.CountryLoader;
import com.pablisco.countryinfo.net.LoaderResponse;
import com.pablisco.countryinfo.widget.DividerItemDecoration;

import java.util.List;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

/**
 * Created by pablisco on 05/02/15.
 *
 * Lists all the countries available
 *
 */
public class CountryListFragment extends Fragment
	implements CountryLoader.LoaderCallbacks {

	/** Main layout to hold the data */
	private RecyclerView recyclerView;
	/** This view provides with the loading widget */
	private LinearLayout loadingLayout;

	@Override public void onResume() {
		super.onResume();
		getLoaderManager().restartLoader(0, null, this).forceLoad();
	}

	@Override public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
	                         @Nullable Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_list_country, container, false);
	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		final FragmentActivity context = getActivity();
		this.recyclerView = (RecyclerView) view.findViewById(android.R.id.list);
		// set up the list divider and the layout manager
		RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
		this.recyclerView.setLayoutManager(layoutManager);
		this.recyclerView.addItemDecoration(new DividerItemDecoration(context, LinearLayoutManager.VERTICAL));
		this.loadingLayout = (LinearLayout) view.findViewById(R.id.progressContainer);
	}

	@Override public Loader<LoaderResponse<List<Country>>> onCreateLoader(int i, Bundle bundle) {
		return new CountryLoader(getActivity());
	}

	@Override public void onLoadFinished(Loader<LoaderResponse<List<Country>>> objectLoader,
	                                     LoaderResponse<List<Country>> response) {
		if (!response.hasErrors()) {
			setListVisible(true);
			CountryAdapter adapter = new CountryAdapter(response.getResult());
			this.recyclerView.setAdapter(adapter);
		} else {
			Toast.makeText(getActivity(), "Error: " + response.getError().getMessage(), Toast.LENGTH_LONG).show();
		}
	}

	@Override
	public void onLoaderReset(Loader<LoaderResponse<List<Country>>> objectLoader) {

	}

	/**
	 * Similarly to {@link ListFragment} this hides or shows the adapter view ({@link ListView}
	 * on the {@link ListFragment}). TODO: apply animation
	 * @param visible
	 */
	public void setListVisible(boolean visible) {
		if (visible) {
			this.recyclerView.setVisibility(VISIBLE);
			this.loadingLayout.setVisibility(GONE);
		} else {
			this.recyclerView.setVisibility(GONE);
			this.loadingLayout.setVisibility(VISIBLE);
		}
	}

}
