package com.pablisco.countryinfo;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBarActivity;

/**
 * Created by pablisco on 05/02/15.
 *
 * This activity includes an instance of {@link CountryDetailsFragment}.
 *
 */
public class DetailsActivity extends ActionBarActivity {

	public static final String TAG_COUNTRY_DETAILS = "com.pablisco.countryinfo.DetailsActivity;TAG_COUNTRY_DETAILS";
	private CountryDetailsFragment detailsFragment;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		final FragmentManager fragmentManager = getSupportFragmentManager();
		detailsFragment = (CountryDetailsFragment) fragmentManager.findFragmentByTag(TAG_COUNTRY_DETAILS);
		if (detailsFragment == null) {
			detailsFragment = new CountryDetailsFragment();
			fragmentManager.beginTransaction()
				.add(android.R.id.content, detailsFragment, TAG_COUNTRY_DETAILS)
				.commit();
		}
		// forward the extras form the activity to the fragment
		detailsFragment.setArguments(getIntent().getExtras());
	}

}
