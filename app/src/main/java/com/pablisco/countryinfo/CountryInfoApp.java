package com.pablisco.countryinfo;

import android.app.Application;

/**
 * Created by pablisco on 05/02/15.
 *
 * This allows us to access the applicatiton context without having a reference to a context.
 *
 * Use with caution.
 *
 */
public class CountryInfoApp extends Application {

	private static CountryInfoApp instance;

	@Override
	public void onCreate() {
		super.onCreate();
		instance = this;
	}

	public static CountryInfoApp app() {
		return instance;
	}

	public int getStatusBarHeight() {
		int result = 0;
		int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
		if (resourceId > 0) {
			result = getResources().getDimensionPixelSize(resourceId);
		}
		return result;
	}

}
