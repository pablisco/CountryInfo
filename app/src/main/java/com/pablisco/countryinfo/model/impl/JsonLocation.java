package com.pablisco.countryinfo.model.impl;

import com.pablisco.countryinfo.model.Location;

import org.json.JSONArray;

/**
 * Created by pablisco on 04/02/15.
 *
 * Json implementation of {@link Location}.
 *
 */
public class JsonLocation extends BaseLocation implements Location {

	/** Use one of the static factory methods provided */
	private JsonLocation() {
	}

	/**
	 * Alternative static factory method for {@link #from(JSONArray)}
	 * @return A new instance of {@link JsonLocation}
	 */
	public static JsonLocation from(long latitude, long longitude) {
		JsonLocation result = new JsonLocation();
		result.latitude = latitude;
		result.longitude = longitude;
		return result;
	}

	/**
	 * Creates a JsonLocation from the provided {@link JSONArray}
	 * @param source Origin of the location
	 * @return A new instance of {@link JsonLocation}
	 */
	public static JsonLocation from(JSONArray source) {
		return from(source.optLong(0), source.optLong(1));
	}
}
