package com.pablisco.countryinfo.model;

import java.util.List;
import java.util.Map;

/**
 * Created by pablisco on 04/02/15.
 *
 * Model interface for a country. It only exposes setters to ensure immutability.
 * <p />
 * The data documentation is extracted partially from https://github.com/fayder/restcountries/wiki/API-1.x.x
 * <p />
 * It maps data returned form http://restcountries.eu/rest/v1/all
 *
 */
public interface Country {

	/** @return the name of the country */
	String getName();

	/** @return the name of the capital of the country */
	String getCapital();

	/** @return the name of the region of the country */
	String getRegion();

	/** @return teh name of the rubregion of the country */
	String getSubregion();

	/**
	 * Alias for {@link #getName()} but providing a translation if it's available.
	 * <p/>
	 * The available languages are en, es, fr, it, de, ja. If the provided language is not provided
	 * the default name provided by {@link #getName()}.
	 * @param language The target language
	 * @return The name of the country in the provided language if translation is available.
	 */
	String getName(String language);

	/** @return A map of translations of the name for serializing usage  */
	Map<String, String> getTranslations();

	/** @return the population of the country */
	long getPopulation();

	/** @return the coordinate location of the country */
	Location getLocation();

	/** @return the name of the people living in the country */
	String getDemonym();

	/** @return The area of the country in square kilometers */
	int getArea();

	/** @return the Gini coefficent of this country which represents the country wealth distribution */
	float getGiniCoefficent();

	/** @return the time zones available in this country */
	List<String> getTimeZones();

	/** @return A list of codes from bordering countries */
	List<String> getBorderCountryCodes();

	/** @return The name given to the country by it's inhavitants */
	String getNativeName();

	/** @return the international calling codes used in this country */
	List<String> getCallingCodes();

	/** @return the top level domains used by this country */
	List<String> getTopLevelDomains();

	/** @return the ISO 3166-1 alpha-2 code for this country */
	String getAlpha2Code();

	/** @return the ISO 3166-1 alpha-3 code for this country */
	String getAlpha3Code();

	/** @return a list of currencies officially used in the country */
	List<String> getCurrencies();

	/** @return a list of languages officially used in the country */
	List<String> getLanguages();

}
