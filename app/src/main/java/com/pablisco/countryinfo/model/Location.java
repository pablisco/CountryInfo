package com.pablisco.countryinfo.model;

/**
 * Created by pablisco on 04/02/15.
 *
 * Represents a location with longitude and latitude
 *
 */
public interface Location {

	/** @return The longitude of the location */
	float getLongitude();

	/** @return The latitude of the location */
	float getLatitude();

}
