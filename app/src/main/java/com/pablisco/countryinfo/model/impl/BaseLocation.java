package com.pablisco.countryinfo.model.impl;

import com.pablisco.countryinfo.model.Location;

/**
 * Created by pablisco on 04/02/15.
 */
public class BaseLocation implements Location {

	protected float longitude;
	protected float latitude;

	protected BaseLocation() {
	}

	public BaseLocation(Location original) {
		longitude = original.getLongitude();
		latitude = original.getLatitude();
	}

	@Override public float getLongitude() {
		return longitude;
	}

	@Override public float getLatitude() {
		return latitude;
	}

}
