package com.pablisco.countryinfo.model.impl;

import com.pablisco.countryinfo.model.Country;
import com.pablisco.countryinfo.model.Location;

import java.util.List;
import java.util.Map;

/**
 * Created by pablisco on 04/02/15.
 */
public class BaseCountry implements Country {

	protected String name;
	protected String capital;
	protected String region;
	protected String subregion;
	protected Map<String, String> nameTranslations;
	protected long population;
	protected List<String> languages;
	protected List<String> currencies;
	protected String alpha3Code;
	protected String alpha2Code;
	protected List<String> topLevelDomains;
	protected Location location;
	protected String denomyn;
	protected int area;
	protected float gini;
	protected List<String> timeZones;
	protected List<String> countryCodes;
	protected String nativeName;
	protected List<String> callingCodes;

	protected BaseCountry() {
	}

	/** copy constructor */
	public BaseCountry(Country source) {
		name = source.getName();
		capital = source.getCapital();
		region = source.getRegion();
		subregion = source.getSubregion();
		nameTranslations = getTranslations();
		population = source.getPopulation();
		languages = source.getLanguages();
		currencies = source.getCurrencies();
		alpha2Code = source.getAlpha2Code();
		alpha3Code = source.getAlpha3Code();
		topLevelDomains = source.getTopLevelDomains();
		location = source.getLocation();
		denomyn = source.getDemonym();
		area = source.getArea();
		gini = source.getGiniCoefficent();
		timeZones = source.getTimeZones();
		countryCodes = source.getBorderCountryCodes();
		nativeName = source.getNativeName();
		callingCodes = source.getCallingCodes();
	}


	@Override public String getName() {
		return name;
	}

	@Override public String getCapital() {
		return capital;
	}

	@Override public String getRegion() {
		return region;
	}

	@Override public String getSubregion() {
		return subregion;
	}

	/**
	 * Tries to fetch the best name given a language
	 */
	@Override public String getName(String language) {
		String result = getName();
		if(nameTranslations.containsKey(language)) {
			result = nameTranslations.get(language);
		}
		return result;
	}

	@Override public Map<String, String> getTranslations() {
		return nameTranslations;
	}

	@Override public long getPopulation() {
		return population;
	}

	@Override public Location getLocation() {
		return location;
	}

	@Override public String getDemonym() {
		return denomyn;
	}

	@Override public int getArea() {
		return area;
	}

	@Override public float getGiniCoefficent() {
		return gini;
	}

	@Override public List<String> getTimeZones() {
		return timeZones;
	}

	@Override public List<String> getBorderCountryCodes() {
		return countryCodes;
	}

	@Override public String getNativeName() {
		return nativeName;
	}

	@Override public List<String> getCallingCodes() {
		return callingCodes;
	}

	@Override public List<String> getTopLevelDomains() {
		return topLevelDomains;
	}

	@Override public String getAlpha2Code() {
		return alpha2Code;
	}

	@Override public String getAlpha3Code() {
		return alpha3Code;
	}

	@Override public List<String> getCurrencies() {
		return currencies;
	}

	@Override public List<String> getLanguages() {
		return languages;
	}

	@Override
	public String toString() {
		return name;
	}
}
