package com.pablisco.countryinfo.model.impl;

import android.os.Parcel;
import android.os.Parcelable;

import com.pablisco.countryinfo.model.Country;
import com.pablisco.countryinfo.model.Location;
import com.pablisco.utils.ParcelUtil;

import static com.pablisco.utils.ParcelUtil.readList;

/**
 * Created by pablisco on 04/02/15.
 *
 * Parcelable version of {@link Country}
 *
 */
public class ParcelableCountry extends BaseCountry implements Parcelable {

	public ParcelableCountry(Country source) {
		super(source);
	}

	private ParcelableCountry(Parcel in) {
		name = in.readString();
		capital = in.readString();
		region = in.readString();
		subregion = in.readString();
		nameTranslations = ParcelUtil.readMap(in);
		population = in.readLong();
		languages = readList(in);
		currencies = readList(in);
		alpha2Code = in.readString();
		alpha3Code = in.readString();
		topLevelDomains = readList(in);
		location = (Location) in.readParcelable(getClass().getClassLoader());
		denomyn = in.readString();
		area = in.readInt();
		gini = in.readFloat();
		timeZones = readList(in);
		countryCodes = readList(in);
		nativeName = in.readString();
		callingCodes = readList(in);
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(name);
		dest.writeString(capital);
		dest.writeString(region);
		dest.writeString(subregion);
		dest.writeMap(nameTranslations);
		dest.writeLong(population);
		dest.writeList(languages);
		dest.writeList(currencies);
		dest.writeString(alpha2Code);
		dest.writeString(alpha3Code);
		dest.writeList(topLevelDomains);
		dest.writeParcelable(new ParcelableLocation(location), flags);
		dest.writeString(denomyn);
		dest.writeInt(area);
		dest.writeFloat(gini);
		dest.writeList(timeZones);
		dest.writeList(countryCodes);
		dest.writeString(nativeName);
		dest.writeList(callingCodes);
	}

	public static Creator<ParcelableCountry> CREATOR = new Creator<ParcelableCountry>() {
		@Override
		public ParcelableCountry createFromParcel(Parcel source) {
			return new ParcelableCountry(source);
		}

		@Override
		public ParcelableCountry[] newArray(int size) {
			return new ParcelableCountry[size];
		}
	};

}
