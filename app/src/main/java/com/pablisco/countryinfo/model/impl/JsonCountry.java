package com.pablisco.countryinfo.model.impl;

import com.pablisco.countryinfo.model.Country;

import org.json.JSONObject;

import static com.pablisco.utils.JsonUtil.arrayToList;
import static com.pablisco.utils.JsonUtil.objectToMap;

/**
 * Created by pablisco on 04/02/15.
 *
 * Json implementation of {@link Country}
 *
 */
public class JsonCountry extends BaseCountry implements Country {

	/**
	 *
	 * Constructs the {@link Country} instance from a {@link JSONObject}
	 *
	 * @param source
	 */
	public JsonCountry(JSONObject source) {
		name = source.optString("name");
		capital = source.optString("capital");
		region = source.optString("region");
		subregion = source.optString("subregion");
		nameTranslations = objectToMap(source.optJSONObject("translations"));
		population = source.optLong("population", -1);
		languages = arrayToList(source.optJSONArray("languages"));
		currencies = arrayToList(source.optJSONArray("currencies"));
		alpha2Code = source.optString("alpha2Code");
		alpha3Code = source.optString("alpha3Code");
		topLevelDomains = arrayToList(source.optJSONArray("topLevelDomain"));
		location = JsonLocation.from(source.optJSONArray("latlng"));
		denomyn = source.optString("demonym");
		area = source.optInt("area");
		gini = (float) source.optDouble("gini");
		timeZones = arrayToList(source.optJSONArray("timezones"));
		countryCodes = arrayToList(source.optJSONArray("borders"));
		nativeName = source.optString("nativeName");
		callingCodes = arrayToList(source.optJSONArray("callingCodes"));
	}

}
