package com.pablisco.countryinfo.model.impl;

import android.os.Parcel;
import android.os.Parcelable;

import com.pablisco.countryinfo.model.Location;

/**
 * Created by pablisco on 04/02/15.
 *
 * Parcelable version of {@link Location}
 *
 */
public class ParcelableLocation extends BaseLocation implements Location, Parcelable {

	public ParcelableLocation(Location original) {
		super(original);
	}

	public ParcelableLocation(Parcel in) {
		latitude = in.readFloat();
		longitude = in.readFloat();
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeFloat(latitude);
		dest.writeFloat(longitude);
	}

	public static Creator<ParcelableLocation> CREATOR = new Creator<ParcelableLocation>() {
		@Override
		public ParcelableLocation createFromParcel(Parcel source) {
			return new ParcelableLocation(source);
		}

		@Override
		public ParcelableLocation[] newArray(int size) {
			return new ParcelableLocation[size];
		}
	};

}
