package com.pablisco.countryinfo;

import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;

import static com.pablisco.countryinfo.CountryInfoApp.app;

/**
 * Created by pablisco on 05/02/15.
 *
 * Entry activity and simply a shell for the first fragment
 *
 */
public class MainActivity extends ActionBarActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
			toolbar.setPadding(0, app().getStatusBarHeight(), 0, 0);
		}
		setSupportActionBar(toolbar);
	}

}
