package com.pablisco.countryinfo.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.pablisco.countryinfo.R;

/**
 * Created by pablisco on 08/02/15.
 *
 * Extension of {@link ImageView} that allows to set a fixed ratio for it's size.
 *
 * De default ratio is 1 so the shape of this view would be a square to change it set the ratio
 * value in the xml layout
 *
 */
public class RatioImageView extends ImageView {


	private float ratio = 1;

	public RatioImageView(Context context) {
		super(context);
	}

	public RatioImageView(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}

	public RatioImageView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		// process attributes
		final TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.RatioImageView);
		for(int i = 0, n = a.length(); i < n;i++) {
			final int attr = a.getIndex(i);
			switch (attr) {
				case R.styleable.RatioImageView_ratio:
					ratio = a.getFloat(attr, ratio);
					break;
			}
		}
		a.recycle();
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		int width = getMeasuredWidth();
		int height = (int) (width / ratio);
		setMeasuredDimension(width, height);
	}
}
