package com.pablisco.countryinfo.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ScrollView;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by pablisco on 07/02/15.
 *
 * Extension of {@link ScrollView} that allows for listening to scroll events.
 *
 */
public class ObservableScrollView extends ScrollView {

	private List<OnScrollListener> listeners = new LinkedList<>();

	public interface OnScrollListener {

		void onScrollChanged(int horizontal, int vertical);

	}

	public ObservableScrollView(Context context) {
		super(context);
	}

	public ObservableScrollView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public ObservableScrollView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
	}

	@Override protected void onScrollChanged(int l, int t, int oldl, int oldt) {
		super.onScrollChanged(l, t, oldl, oldt);
		for(OnScrollListener listener : listeners) {
			listener.onScrollChanged(l, t);
		}
	}

	public void registerScrollListener(OnScrollListener listener) {
		listeners.add(listener);
	}

	public void unregisterScrollListener(OnScrollListener listener) {
		listeners.remove(listener);
	}

}
