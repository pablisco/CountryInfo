package com.pablisco.countryinfo.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.pablisco.countryinfo.CountryDetailsFragment;
import com.pablisco.countryinfo.DetailsActivity;
import com.pablisco.countryinfo.R;
import com.pablisco.countryinfo.model.Country;
import com.pablisco.countryinfo.model.impl.ParcelableCountry;
import com.pablisco.utils.ImageCacheUtil;

import java.util.List;
import java.util.Locale;

/**
 * Created by pablisco on 07/02/15.
 *
 * Adapter to provide views based on {@link com.pablisco.countryinfo.model.Country}
 *
 */
public class CountryAdapter extends RecyclerView.Adapter<CountryAdapter.ViewHolder> {

	private List<Country> countries;

	public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

		private TextView countryNameView;
		private ImageView flagView;
		private Country country;

		public ViewHolder(View itemView) {
			super(itemView);
			itemView.setOnClickListener(this);
			this.countryNameView = (TextView) itemView.findViewById(R.id.country_name_view);
			this.flagView = (ImageView) itemView.findViewById(R.id.flag_view);
		}

		public void setCountry(Country country) {
			this.country = country;
			String name = country.getName(Locale.getDefault().getLanguage());
			this.countryNameView.setText(name);
			ImageCacheUtil.attachUrlImage(this.flagView, flagPathFor(country));
		}

		@Override
		public void onClick(View v) {
			if (country != null) {
				final Context context = v.getContext();
				final Intent intent = new Intent(context, DetailsActivity.class);
				ParcelableCountry extraCountry = new ParcelableCountry(country);
				intent.putExtra(CountryDetailsFragment.EXTRA_COUNTRY, extraCountry);
				context.startActivity(intent);
			}
		}
	}

	/**
	 * Generates a flag path for a given country.
	 * @param country We get the alpha-2 country code to generate the url
	 * @return a url from http://www.geonames.org
	 */
	public static String flagPathFor(Country country) {
		String countryCode = country.getAlpha2Code();
		return "http://www.geonames.org/flags/x/" + countryCode.toLowerCase() + ".gif";
	}

	public CountryAdapter(List<Country> countries) {
		this.countries = countries;
	}

	@Override
	public ViewHolder onCreateViewHolder(ViewGroup parent, int i) {
		final LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
		final View view = layoutInflater.inflate(R.layout.view_list_country, parent, false);
		return new ViewHolder(view);
	}

	@Override
	public void onBindViewHolder(ViewHolder viewHolder, int i) {
		viewHolder.setCountry(countries.get(i));
	}

	@Override
	public int getItemCount() {
		return countries.size();
	}

}
