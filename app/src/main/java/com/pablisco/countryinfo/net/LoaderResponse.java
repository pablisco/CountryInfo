package com.pablisco.countryinfo.net;

import android.support.v4.content.Loader;

/**
 * Created by pablisco on 05/02/15.
 *
 * Simple wrapper to return either a response or an exception. Used with {@link Loader} instances
 *
 */
public class LoaderResponse<T> {

	private Throwable error;
	private T result;

	private LoaderResponse() {
	}

	public static <T> LoaderResponse<T> error(Throwable error) {
		LoaderResponse<T> response = new LoaderResponse<>();
		response.error = error;
		return response;
	}
	public static <T> LoaderResponse<T> result(T result) {
		LoaderResponse<T> response = new LoaderResponse<>();
		response.result = result;
		return response;
	}

	public Throwable getError() {
		return error;
	}

	public T getResult() {
		return result;
	}

	public boolean hasErrors() {
		return error != null;
	}

}
