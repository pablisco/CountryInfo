package com.pablisco.countryinfo.net;

import android.content.Context;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.AsyncTaskLoader;

import com.pablisco.countryinfo.model.Country;
import com.pablisco.countryinfo.model.impl.JsonCountry;

import org.json.JSONArray;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by pablisco on 05/02/15.
 *
 * Loads all the available countries from the following url:
 * <p />
 * http://restcountries.eu/rest/v1/all
 *
 */
public class CountryLoader extends AsyncTaskLoader<LoaderResponse<List<Country>>> {

	public static final String COUNTRY_URL = "http://restcountries.eu/rest/v1/all";
	public static final String COUNTRY_BY_ID_URL = "http://restcountries.eu/rest/v1/alpha?codes=";

	private String scheduledUrl = COUNTRY_URL;

	public static interface LoaderCallbacks
		extends LoaderManager.LoaderCallbacks<LoaderResponse<List<Country>>> {

	}

	public CountryLoader(Context context) {
		super(context);
	}

	public void setScheduledUrl(String scheduledUrl) {
		this.scheduledUrl = scheduledUrl;
	}

	@Override
	public LoaderResponse<List<Country>> loadInBackground() {
		LoaderResponse<List<Country>> result;
		try {
			URL url = new URL(scheduledUrl);
			final HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
			urlConnection.setRequestMethod("GET");
			urlConnection.setInstanceFollowRedirects(true);
			urlConnection.connect();
			int statusCode = urlConnection.getResponseCode();
			if (statusCode >= 200 && statusCode < 300) {
				final InputStreamReader streamReader = new InputStreamReader(urlConnection.getInputStream());
				BufferedReader reader = new BufferedReader(streamReader);
				StringBuilder content = new StringBuilder();
				String line;
				while ((line = reader.readLine()) != null) {
					content.append(line).append('\n');
				}
				reader.close();
				final JSONArray json = new JSONArray(content.toString());
				List<Country> response = new LinkedList<>();
				for(int i =0, n = json.length(); i < n;i++) {
					Country item = new JsonCountry(json.getJSONObject(i));
					response.add(item);
				}
				result = LoaderResponse.result(response);
			} else {
				result = LoaderResponse.error(new Exception("Status code error: " + statusCode));
			}
		} catch (Exception e) {
			e.printStackTrace();
			result = LoaderResponse.error(e);
		}

		return result;
	}

}
