package com.pablisco.countryinfo;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.Loader;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.pablisco.countryinfo.adapters.CountryAdapter;
import com.pablisco.countryinfo.model.Country;
import com.pablisco.countryinfo.model.Location;
import com.pablisco.countryinfo.model.impl.ParcelableCountry;
import com.pablisco.countryinfo.net.CountryLoader;
import com.pablisco.countryinfo.net.LoaderResponse;
import com.pablisco.countryinfo.widget.ObservableScrollView;
import com.pablisco.utils.ImageCacheUtil;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

import static com.pablisco.countryinfo.CountryInfoApp.app;

/**
 * Created by pablisco on 05/02/15.
 * <p/>
 * This fragment will inflate a details view and populate the view with the provided {@link Country}
 */
public class CountryDetailsFragment extends Fragment implements ObservableScrollView.OnScrollListener, CountryLoader.LoaderCallbacks {

	public static final String EXTRA_COUNTRY = "com.pablisco.countryinfo.CountryDetailsFragment;EXTRA_COUNTRY";
	public static final String EXTRA_COUNTRY_ID = "com.pablisco.countryinfo.CountryDetailsFragment;EXTRA_COUNTRY_ID";

	private final String TAG = CountryDetailsFragment.class.getSimpleName();
	private Country country;

	private ImageView flagView;
	private Toolbar toolbar;
	private TextView capitalCityView;
	private TextView nativeNameView;
	private TextView currenciesView;
	private TextView giniView;
	private TextView languagesView;
	private ImageView mapView;
	private TextView demonymView;
	private TextView populationView;
	private LinearLayout timesView;
	private TextView regionView;
	private TextView topLevelDomainView;
	private LinearLayout borderView;
	private ObservableScrollView observableScrollView;
	private int headerScrollOffset;
	private FrameLayout headerLayout;
	private View progressContainer;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_details_country, container, false);
	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		flagView = (ImageView) view.findViewById(R.id.flag_view);
		capitalCityView = (TextView) view.findViewById(R.id.capital_city_view);
		nativeNameView = (TextView) view.findViewById(R.id.native_name_view);
		currenciesView = (TextView) view.findViewById(R.id.currencies_view);
		giniView = (TextView) view.findViewById(R.id.gini_view);
		languagesView = (TextView) view.findViewById(R.id.languages_view);
		mapView = (ImageView) view.findViewById(R.id.map_view);
		demonymView = (TextView) view.findViewById(R.id.demonym_view);
		populationView = (TextView) view.findViewById(R.id.population_view);
		timesView = (LinearLayout) view.findViewById(R.id.times_view);
		regionView = (TextView) view.findViewById(R.id.region_view);
		topLevelDomainView = (TextView) view.findViewById(R.id.top_level_domain_view);
		borderView = (LinearLayout) view.findViewById(R.id.border_view);
		observableScrollView = (ObservableScrollView) view.findViewById(R.id.observable);
		observableScrollView.registerScrollListener(this);
		headerLayout = (FrameLayout) view.findViewById(R.id.header_layout);
		headerLayout.post(new Runnable() {
			@Override
			public void run() {
				headerScrollOffset = flagView.getMeasuredHeight() - toolbar.getMeasuredHeight();
				// this will add a padding to the top reflecting the height of the header layout
				observableScrollView.setClipToPadding(false);
				int bottom = observableScrollView.getPaddingBottom();
				int right = observableScrollView.getPaddingRight();
				int top = observableScrollView.getPaddingTop() + headerLayout.getMeasuredHeight();
				int left = observableScrollView.getPaddingLeft();
				observableScrollView.setPadding(left, top, right, bottom);
				if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
					toolbar.getBackground().setAlpha(55);
				}
			}
		});
		progressContainer = view.findViewById(R.id.progressContainer);
		// setup toolbar
		toolbar = (Toolbar) view.findViewById(R.id.toolbar);
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
			toolbar.setPadding(0, app().getStatusBarHeight(), 0, 0);
		}
		toolbar.setNavigationIcon(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
		if (getActivity() instanceof ActionBarActivity) {
			((ActionBarActivity) getActivity()).setSupportActionBar(toolbar);
		} else {
			throw new IllegalStateException("Should be inside an ActionBarActivity");
		}
		// TODO: gather views
	}

	/**
	 * Concatenates the list of items with the provided connector in between the items
	 * @param items to be concatenated
	 * @param connector to be put between the items
	 * @return a new {@link CharSequence} with the items concatenated
	 */
	private CharSequence concat(Iterable<String> items, CharSequence connector) {
		StringBuilder result = new StringBuilder();
		if (items != null) {
			final Iterator<String> it = items.iterator();
			while (it.hasNext()) {
				result.append(it.next());
				// the last item doesn't need a connector
				if (it.hasNext()) {
					result.append(connector);
				}
			}
		}
		return result;
	}

	/**
	 * Creates a series of views for the provided items which are inserted in the provided container.
	 * The container is emptied before the new views are added. It also sets the provided text as
	 * a tag of the view for future reference. (i.e. handing click events)
	 * @param items The items to generate the layout
	 * @param container the container that will hold the created views
	 * @return the list of views provided for future processing
	 */
	private List<View> populateLayout(Iterable<String> items, ViewGroup container) {
		List<View> result = new LinkedList<>();
		if (items != null && items.iterator().hasNext()) {
			LayoutInflater layoutInflater = LayoutInflater.from(container.getContext());
			container.removeAllViews();
			for (String item : items) {
				// inflate and add to the container
				View view = layoutInflater.inflate(R.layout.view_embed_list_item, container, false);
				container.addView(view);
				view.setTag(R.id.target_string, item);
				TextView textView = (TextView) view.findViewById(android.R.id.text1);
				textView.setText(item);
				result.add(view);
			}
		}
		return result;
	}

	/**
	 * Sets the country of the fragment and populates the relevant views.
	 *
	 * @param country
	 */
	public void setCountry(final Country country) {
		this.country = country;
		progressContainer.setVisibility(View.GONE);
		observableScrollView.setVisibility(View.VISIBLE);
		// set up name depending of the locale
		final Locale locale = Locale.getDefault();
		String countryName = country.getName(locale.getLanguage());
		getActivity().setTitle(countryName);
		String url = CountryAdapter.flagPathFor(country);
		ImageCacheUtil.attachUrlImage(flagView, url);
		capitalCityView.setText(country.getCapital());
		nativeNameView.setText(country.getNativeName());
		currenciesView.setText(concat(country.getCurrencies(), ", "));
		giniView.setText(Float.toString(country.getGiniCoefficent()));
		languagesView.setText(concat(country.getLanguages(), ", "));
		// this makes sure the view is measured
		mapView.post(new Runnable() {
			@Override public void run() {
				int width = mapView.getMeasuredWidth();
				int height = mapView.getMeasuredHeight();
				String mapUrl = createMapUrl(country.getLocation(), width, height);
				ImageCacheUtil.attachUrlImage(mapView, mapUrl);
			}
		});
		demonymView.setText(country.getDemonym());
		DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.getDefault());
		populationView.setText(formatter.format(country.getPopulation()));
		final List<String> timeZones = country.getTimeZones();
		// TODO format time
		populateLayout(timeZones, timesView);
		StringBuffer regionText = new StringBuffer(country.getRegion());
		final String subregion = country.getSubregion();
		if (subregion != null && !subregion.isEmpty()) {
			regionText.append(", ").append(subregion);
		}
		regionView.setText(regionText);
		final List<String> topLevelDomains = country.getTopLevelDomains();

		topLevelDomainView.setText(concat(topLevelDomains, ", "));

		final List<String> borderCountryCodes = country.getBorderCountryCodes();
		if (!borderCountryCodes.isEmpty()) {
			final List<View> borderViews = populateLayout(borderCountryCodes, borderView);
			View.OnClickListener borderClickListener = new View.OnClickListener() {
				@Override public void onClick(View v) {
					final String targetString = (String)v.getTag(R.id.target_string);
					Intent intent = new Intent(getActivity(), DetailsActivity.class);
					intent.putExtra(EXTRA_COUNTRY_ID, targetString);
					startActivity(intent);
					// TODO: handle clicks
				}
			};
			for (View view : borderViews) {
				view.setClickable(true);
				view.setBackgroundResource(R.drawable.abc_list_selector_holo_light);
				view.setOnClickListener(borderClickListener);
			}
		}

	}

	private String createMapUrl(Location location, int width, int height) {
		return "http://maps.google.com/maps/api/staticmap?center=" + location.getLatitude() + ","
			+ location.getLongitude() + "&zoom=6&size=" + width + "x" + height + "&sensor=false";
	}

	@Override
	public void onResume() {
		super.onResume();
		final Bundle arguments = getArguments();
		if (arguments == null) {
			throw new IllegalArgumentException("No arguments were provided to " + TAG);
		} else if (arguments.containsKey(EXTRA_COUNTRY)) {
			ParcelableCountry country = arguments.getParcelable(EXTRA_COUNTRY);
			setCountry(country);
		} else if (arguments.containsKey(EXTRA_COUNTRY_ID)) {
			getLoaderManager().initLoader(0, arguments, this).forceLoad();
		} else {
			throw new IllegalArgumentException("Need to provide a valid argument to " + TAG);
		}
	}


	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		boolean result;
		if (item.getItemId() == android.R.id.home) {
			getActivity().finish();
			result = true;
		} else {
			result = super.onOptionsItemSelected(item);
		}
		return result;
	}

	@Override
	public void onScrollChanged(int horizontal, int vertical) {
		// this will not work with Anything below HC, although the app wills till be usable.
		// This is just to make it pretty :)
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			int scrolled = headerScrollOffset - vertical;
			int alpha = 255;
			if (scrolled > 0) {
				float process = scrolled / (float)headerScrollOffset;
				alpha -= 200 * process;
			}
			toolbar.getBackground().setAlpha(alpha);
			ViewCompat.setTranslationY(headerLayout, -(Math.min(headerScrollOffset, vertical)));
		}
	}

	@Override
	public Loader<LoaderResponse<List<Country>>> onCreateLoader(int id, Bundle args) {
		CountryLoader loader = new CountryLoader(getActivity());
		loader.setScheduledUrl(CountryLoader.COUNTRY_BY_ID_URL + args.getString(EXTRA_COUNTRY_ID));
		return loader;
	}

	@Override
	public void onLoadFinished(Loader<LoaderResponse<List<Country>>> loader, LoaderResponse<List<Country>> data) {
		if (data.hasErrors()) {
			// TODO: report
		} else {
			final List<Country> result = data.getResult();
			if (result.isEmpty()) {
				// TODO: report
			} else {
				setCountry(result.get(0));
			}
		}
	}

	@Override
	public void onLoaderReset(Loader<LoaderResponse<List<Country>>> loader) {

	}
}
