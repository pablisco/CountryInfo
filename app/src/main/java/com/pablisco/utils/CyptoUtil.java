package com.pablisco.utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by pablisco on 07/02/15.
 *
 * Util to do cryptography tasks.
 *
 */
public class CyptoUtil {

	public static String md5(final String subject) {
		StringBuilder result = new StringBuilder();
		final String MD5 = "MD5";
		try {
			// Create MD5 Hash
			MessageDigest digest = MessageDigest.getInstance(MD5);
			digest.update(subject.getBytes());
			byte messageDigest[] = digest.digest();

			// Parse hash to string
			for (byte aMessageDigest : messageDigest) {
				result.append(String.format("%02X", aMessageDigest & 0xFF));
			}
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return result.toString();
	}

}
