package com.pablisco.utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by pablisco on 04/02/15.
 *
 * Utils for Json parsing.
 *
 */
public class JsonUtil {

	private JsonUtil() {
	}

	/**
	 * Converts a {@link JSONObject} into a {@link Map}. If one of the values in the object is not
	 * of the type {@link V} an exception will be thrown. If multiple types are used it's safer to
	 * use {@link Object} as the generics value type.
	 *
	 *
	 * @param object Source of the map
	 * @param <V> The type expected for the map values
	 * @return A fresh new Map with the values from the {@link JSONObject}. Empty if the object
	 * provided is null
	 * @throws ClassCastException
	 */
	@SuppressWarnings("unchecked")
	public static <V> Map<String, V> objectToMap(JSONObject object) throws ClassCastException {
		Map<String, V> result = new HashMap<>();
		if (object != null) {
			final Iterator<String> it = object.keys();
			while (it.hasNext()) {
				final String key = it.next();
				result.put(key, (V) object.opt(key));
			}
		}
		return result;
	}

	/**
	 * Converts a {@link JSONArray} into a {@link List}. If one of the values in the object is not
	 * of the type {@link V} an exception will be thrown. If multiple types are used it's safer to
	 * use {@link Object} as the generics value type
	 *
	 * @param array Source of the {@link List}
	 * @param <V> The type expected for the list entries
	 * @return A fresh new {@link List} with the values from the {@link JSONArray}. Empty if the
	 * array provided is null.
	 * @throws ClassCastException
	 */
	@SuppressWarnings("unchecked")
	public static <V> List<V> arrayToList(JSONArray array) throws ClassCastException {
		List<V> result = new LinkedList<>();
		if (array != null) {
			for (int i = 0, n = array.length(); i < n; i++) {
				result.add((V) array.opt(i));
			}
		}
		return result;
	}

}
