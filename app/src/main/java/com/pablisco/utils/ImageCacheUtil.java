package com.pablisco.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.widget.ImageView;
import android.widget.Toast;

import com.pablisco.countryinfo.CountryInfoApp;
import com.pablisco.countryinfo.R;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by pablisco on 05/02/15.
 *
 * This factory allows images to be loaded in the background is not available. It will defer the
 * loading into an {@link AsyncTask} if the is no cache entry
 *
 */
public class ImageCacheUtil {

	private static File cacheLocation;

	/**
	 *
	 * Either we load the image from disk or we start an background
	 *
	 * @param imageView the target {@link ImageView}
	 * @param url The image to load
	 */
	public static void attachUrlImage(final ImageView imageView, final String url) {
		String md5 = CyptoUtil.md5(url);
		final File location = new File(getCacheLocation(), md5);
		if (location.exists()) {
			// if the image is cached we just pass it forward
			Drawable result = Drawable.createFromPath(location.getAbsolutePath());
			imageView.setImageDrawable(result);
		} else {
			imageView.setImageDrawable(null);
			// We save a tag to ensure the right image is set
			imageView.setTag(R.id.cache_image_target, url);
			// This async task will load the image
			final AsyncTask<Void, Void, Drawable> task = new AsyncTask<Void, Void, Drawable>() {

				@Override
				protected Drawable doInBackground(Void... params) {
					HttpURLConnection connection;
					Drawable result = null;
					InputStreamReader input = null;
					FileOutputStream output = null;
					try {
						connection = (HttpURLConnection) new URL(url).openConnection();
						connection.setRequestMethod("GET");
						connection.connect();
						int statusCode = connection.getResponseCode();
						if (statusCode >= 200 && statusCode < 300) {
							// we have a good response so let's parse it and save it
							input = new InputStreamReader(connection.getInputStream());
							output = new FileOutputStream(location);
							final Bitmap bitmap = BitmapFactory.decodeStream(connection.getInputStream());
							bitmap.compress(Bitmap.CompressFormat.PNG, 90, output);
							// TODO: in a real implementation we need to resize the image to avoid
							// Out of memory issues
							result = Drawable.createFromPath(location.getAbsolutePath());
						} else {
							final Exception error = new Exception("Status code error: " + statusCode);
							reportError(imageView.getContext(), error);
						}
					} catch (IOException e) {
						reportError(imageView.getContext(), e);
					} finally {
						// let's close any resources we have
						try {
							if (input != null) {
								input.close();
							}
							if (output != null) {
								output.close();
							}
						} catch (Exception e) {
							//no op
						}
					}
					return result;
				}

				@Override
				protected void onPostExecute(Drawable result) {
					String targetUrl = (String) imageView.getTag(R.id.cache_image_target);
					// we only update the image view if this was the last task to be run on the
					// image to avoid false images.
					if (url.equals(targetUrl)) {
						imageView.setImageDrawable(result);
					}
				}
			};
			// ensure we run tasks in parallel
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
				task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
			} else {
				task.execute();
			}
		}
	}

	private static void reportError(Context context, Throwable throwable) {
		// TODO: report error better
		Toast.makeText(context, throwable.getMessage(), Toast.LENGTH_SHORT).show();
		throwable.printStackTrace();
	}

	public static File getCacheLocation() {
		if (cacheLocation == null) {
			// TODO:  Ideally we want to handle external storage and remove old pictures
			cacheLocation = new File(CountryInfoApp.app().getCacheDir(), ".images");
			// ensure the cache folder exists
			if(!cacheLocation.exists()) {
				cacheLocation.mkdirs();
			}
		}
		return cacheLocation;
	}
}
