package com.pablisco.utils;

import android.os.Parcel;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by pablisco on 05/02/15.
 *
 * Utils for parcelable objects
 *
 */
public class ParcelUtil {

	private ParcelUtil() {
	}

	public static <T> List<T> readList(Parcel in) {
		List<T> result = new LinkedList<>();
		in.readList(result, ParcelUtil.class.getClassLoader());
		return result;
	}

	public static <K, V> Map<K, V> readMap(Parcel in) {
		Map<K, V> result = new HashMap<>();
		in.readMap(result, ParcelUtil.class.getClassLoader());
		return result;
	}


}
