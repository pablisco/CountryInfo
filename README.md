## Country Info App

A simple app to show a list of countries from http://restcountries.eu/

### How to run the project:

TODO

### Design decisions:

- The model is implemented as a set of interfaces to allow for future extension. This way we can have the same interface adapters and doesn't matter whether the data is from a json object, from the database, or a parcelable.
- The json implementations could extend a json class to serialise the objects back. But this implementation doesn't require upstream communications.

### Code style:

The style of the code follows the same as Google proposes here:

 http://google-styleguide.googlecode.com/svn/trunk/javaguide.html

With the exceptions of indentation, tabs are used instead of spaces in Java but space for xml and for aligning a line. The third answer here is the best one I've found: http://programmers.stackexchange.com/questions/57/tabs-versus-spaces-what-is-the-proper-indentation-character-for-everything-in-e

Although, in the end all comes down to team convention :)
